package com.hoyski.pncqfxfixer;

import junit.framework.TestCase;

public class OutputFilenameTest extends TestCase {
	
	public void testCreateOutputFilename1()
	{
		String inName = "somefile.qfx";
		
		String outNameExpected = "somefile.fixed.qfx";
		
		String outNameActual = PncQfxFixer.createOutputFilename(inName);
		
		assertEquals(outNameExpected, outNameActual);
	}

	public void testCreateOutputFilename2()
	{
		String inName = "somefileqfx";
		
		String outNameExpected = "somefileqfx.fixed";
		
		String outNameActual = PncQfxFixer.createOutputFilename(inName);
		
		assertEquals(outNameExpected, outNameActual);
	}
}
