package com.hoyski.pncqfxfixer;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.StringWriter;

import junit.framework.TestCase;

public class FixTest extends TestCase
{

	public FixTest()
	{
		// TODO Auto-generated constructor stub
	}

	public FixTest(String name)
	{
		super(name);
	}

	/**
	 * Test that the output file has as many lines as the input file
	 * 
	 * @throws Exception
	 */
	public void testFixNumLinesSame() throws Exception
	{
		int numInLines = 0;

		// First count the number of input lines
		BufferedReader inputReader = new BufferedReader(
				new InputStreamReader(getClass().getResourceAsStream("/quickenExport.QFX")));

		while (inputReader.readLine() != null)
		{
			numInLines++;
		}
		inputReader.close();

		// Now re-open the file and pass it to fix()
		inputReader = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream("/quickenExport.QFX")));

		StringWriter writer = new StringWriter();
		BufferedWriter outputWriter = new BufferedWriter(writer);

		PncQfxFixer fixer = new PncQfxFixer();

		fixer.fix(inputReader, outputWriter);

		int numOutLines = writer.toString().split("\r\n|\r|\n").length;

		assertEquals("Number of input lines not equal to number of output lines", numInLines, numOutLines);

	}

	public void testCreateOutputFilename()
	{
		String inFilename = "test.QFX";
		String expectedOutFilename = "test.fixed.QFX";

		assertEquals("createOutputFilename didn't create expected name", expectedOutFilename,
				PncQfxFixer.createOutputFilename(inFilename));
	}

	public void testCreateOutputFilenameWithoutExtension()
	{
		String inFilename = "test";
		String expectedOutFilename = "test.fixed";

		assertEquals("createOutputFilename didn't create expected name", expectedOutFilename,
				PncQfxFixer.createOutputFilename(inFilename));
	}


}
