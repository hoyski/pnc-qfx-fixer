package com.hoyski.pncqfxfixer;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * PncQfxFixer reads a .QFX file and writes a new one with new FITIDs (Financial Institution Transaction IDs). The new
 * FITIDs are computed using the posting date, transaction amount and payee name. Therefore the FITID will be the same
 * for the same transaction every time PncQfxFixer is run, even across multiple QFX files.
 * 
 * Rationale: Per Quicken's <a href=
 * "https://quicken.intuit.com/support/help/bank-download-issues/about-the-fitid-requirements-for-quicken/GEN82245.html">
 * documentation</a> an FITID is supposed to uniquely identify a transaction. If the same transaction is exported in two
 * different QFX files it should have the same FITID in each file. However, in some situations PNC changes the FITID of
 * an already downloaded transaction. The problem appears to occur when a new transaction is posted on a date for which
 * a QFX file has already been generated. PNC's FITIDs are sequentially numbered within a transaction date. A new
 * transaction often is listed first and thus gets an FITID ending in 1 even though that FITID has been previously
 * downloaded and associated with a different transaction.
 * 
 * By computing the FITID from the date, amount and name on the transaction, the FITIDs remain the same across multiple
 * downloads.
 * 
 * @author Dave Hoy (hoyski@gmail.com)
 *
 */
public class PncQfxFixer
{

	public static final String TAG_FITID = "<FITID>";
	public static final String TAG_DTPOSTED = "<DTPOSTED>";
	public static final String TAG_TRNAMT = "<TRNAMT>";
	public static final String TAG_NAME = "<NAME>";

	List<String> currentTransaction = new ArrayList<String>();

	List<String> computedFITIDs = new ArrayList<String>();

	public static void main(String[] args)
	{
		if (args == null || args.length != 1)
		{
			System.out.println("Usage: java -jar PncQfxFixer.jar <QFX_File_to_Fix>");
			System.exit(1);
		}

		String inputFilename = args[0];
		String outputFilename = createOutputFilename(inputFilename);

		try
		{
			BufferedReader inReader = new BufferedReader(new FileReader(inputFilename));
			BufferedWriter outWriter = new BufferedWriter(new FileWriter(outputFilename));
			
			PncQfxFixer fixer = new PncQfxFixer();

			fixer.fix(inReader, outWriter);
			
			outWriter.flush();
			
			inReader.close();
			outWriter.close();

			System.exit(0);
		}
		catch (Exception ex)
		{
			String errorMessage = "Caught unexpected " + ex.getClass().getName();
			System.err.println(errorMessage);
			System.exit(2);
		}
	}

	/**
	 * Fixes the FITIDs in the file with which this instance was created.
	 * 
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public void fix(BufferedReader inReader, BufferedWriter outWriter) throws FileNotFoundException, IOException
	{
		String curLine;
		boolean inTransaction = false;

		while ((curLine = inReader.readLine()) != null)
		{
			if (curLine.equals("<STMTTRN>"))
			{
				// Start of new transaction. Turn on 'inTransaction' and store
				// the line
				inTransaction = true;
				currentTransaction.add(curLine);
			}
			else if (curLine.equals("</STMTTRN>"))
			{
				// End of transaction. Write it out to the file and turn off
				// 'inTransaction'
				currentTransaction.add(curLine);
				writeCurrentTransaction(outWriter);
				inTransaction = false;
			}
			else if (inTransaction)
			{
				// In a transaction. Add the line to the list
				currentTransaction.add(curLine);
			}
			else
			{
				// Not in a transaction. Just copy the line to the output file
				outWriter.write(curLine);
				outWriter.newLine();
			}
		}
		
		outWriter.flush();
	}

	/**
	 * Computes the new FITID for the current transaction and writes the whole transaction to the output file, replacing
	 * the FITID entry with the newly computed value.
	 * 
	 * @param outWriter
	 * @throws IOException
	 */
	public void writeCurrentTransaction(BufferedWriter outWriter) throws IOException
	{

		String newFITID = computeNewFITID();

		for (String line : currentTransaction)
		{
			if (line.startsWith(TAG_FITID))
			{
				// Write the new FITID in place of the old one
				outWriter.write(TAG_FITID + newFITID);
			}
			else
			{
				outWriter.write(line);
			}
			outWriter.newLine();
		}

		currentTransaction.clear();
	}

	/**
	 * Compute a new FITID using hash values of the date posted, vendor name and transaction amount.
	 * 
	 * @return See above
	 */
	public String computeNewFITID()
	{
		String datePosted = getTagValue(TAG_DTPOSTED).substring(0, 8);
		int nameHash = Math.abs(getTagValue(TAG_NAME).hashCode());
		int amountHash = Math.abs(getTagValue(TAG_TRNAMT).hashCode());

		String formatStr = "%09d";

		int counter = 0;

		String newFITID;

		// Generate new FITIDs with a counter on the end until a unique FITID is
		// generated. Otherwise, duplicate
		// FITIDs occur when the same amount is charged at the same vendor twice
		// or more in one day
		do
		{
			newFITID = datePosted + "0" + String.format(formatStr, nameHash) + "0"
					+ String.format(formatStr, amountHash) + (counter == 0 ? "" : Integer.toString(counter));
			counter++;
		}
		while (computedFITIDs.contains(newFITID));

		computedFITIDs.add(newFITID);

		return newFITID;
	}

	/**
	 * Searches through the current transaction and returns the value of the element with the tag 'tagName'. Returns
	 * empty string if the tag is not found.
	 * 
	 * @param tagName
	 * @return
	 */
	public String getTagValue(String tagName)
	{
		for (String entry : currentTransaction)
		{
			if (entry.startsWith(tagName))
			{
				return entry.substring(tagName.length());
			}
		}

		// Didn't find the tag
		return "";
	}

	/**
	 * Construct the filename used for output based on the input filename. This method adds the string ".fixed" before
	 * the file's extension.
	 */
	public static String createOutputFilename(String inputFile)
	{
		int lastDotPos = inputFile.lastIndexOf(".");

		if (lastDotPos != -1)
		{
			return inputFile.substring(0, lastDotPos) + ".fixed" + inputFile.substring(lastDotPos);
		}
		else
		{
			// No dot in the filename. Just add ".fixed" on the end
			return inputFile + ".fixed";
		}
	}
}
