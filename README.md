# PNC QFX Fixer Readme #

PNC QFX Fixer corrects a bug with Quicken QFX files exported from PNC's website. The bug is that the same transactions often get different unique identifiers, or FITIDs, from one download to the next. The Fixer is a simple application that reads a QFX file and generates new FITIDs. The new IDs are based on the date, amount and payee name and are thus the same across files.

To build PNC QFX Fixer, clone the repo and compile the source using Maven. The result will be an executable JAR file. 

To run the fixer, run

```
#!java

java -jar <fully qualified path to JAR file name> <fully qualified path to exported QFX file name>
```

PNC QFX Fixer will create a new QFX file with ".fixed" added before the extension. E.g. Fixing quickenExport.qfx will create a new file named quickenExport.fixed.qfx.

### Integration with Windows Explorer ###

Once the JAR file is built, edit the file WindowsRegistry\AssociatePncQfxFixer.reg. Change the paths to java.exe and the JAR file to match the paths on your machine then use regedit to import the file into the Windows registry. Now when you right click on a .QFX file in Windows Explorer you'll see the option "Apply PNC QFX Fixer".

### Author ###

This program was written by Dave Hoy. All comments, questions and feedback are welcome at hoyski@gmail.com